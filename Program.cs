﻿using System;
using Laba7__HashTable_;

namespace TestEnviroment
{
    class Program
    {
        static void Main(string[] args)
        {
            day[] ex_arr_day = new day[7];
            HashTable hashTable = new HashTable();
            string ex_name;
            string ex_adress;
            for (int i = 0; i < 12; i++)
            {
                ex_name = $"{i}tolovaya";
                ex_adress = $"{12-i}0";
                //ex_adress = $"1{i}";
                Console.WriteLine($"add- { ex_name}  { ex_adress}");
                hashTable.Add(new TOP { name = ex_name, adress = ex_adress, arr_day = ex_arr_day });
                hashTable.Print();
            }
            hashTable.Delete(new TOP { name = "test_name", adress = "test_adress", arr_day = ex_arr_day });
            Console.WriteLine(hashTable.Search(new TOP { name = "0tolovaya", adress = "120", arr_day = ex_arr_day }));
            Console.WriteLine(hashTable.Search(new TOP { name = "5tolovaya", adress = "70", arr_day = ex_arr_day }));
            for (int i = 0; i < 12; i++)
            {
                if (i == 8)
                {
                    ex_name = $"{i}tolovaya";
                }
                ex_name = $"{i}tolovaya";
                //ex_adress = $"1{i}";
                ex_adress = $"{12 - i}0";
                //ex_adress = "10";
                Console.WriteLine($"delete- { ex_name}  { ex_adress}");
                hashTable.Delete(new TOP { name = ex_name, adress = ex_adress, arr_day = ex_arr_day });
                hashTable.Print();
            }
            hashTable.Print();
        }

    }
}