﻿using System;

namespace Laba7__HashTable_
{

    public struct day //часы и минуты начала и конца
    {
        public int hs;
        public int he;
        public int ms;
        public int me;
    };

    public class TOP
    {
        public string name { get; set; }
        public string adress { get; set; }
        public day[] arr_day = new day[7]; //!нет гета и нет сета
        public static bool operator ==(TOP top1, TOP top2) => !(top1 is null) && !(top2 is null) && top1.name + top1.adress == top2.name + top2.adress;
        public static bool operator !=(TOP top1, TOP top2) => !(top1 is null) && !(top2 is null) && top1.name + top1.adress != top2.name + top2.adress;
        public TOP()
        {
            name = "";
            adress = "";
            for (int i = 0; i < 7; i++)
            {
                arr_day[i].hs = 0;
                arr_day[i].he = 0;
                arr_day[i].ms = 0;
                arr_day[i].me = 0;
            }
        }
    }

    public class HashTableNode
    {
        public TOP data { get; set; }
        public int state { get; set; }
        public string key { get; set; }
        public HashTableNode()
        {
            data = new TOP();
            state = 0;
            key = "";
        }
        public override string ToString()
        {
            return $"{data.name} {data.adress}\t {state}"; //!тут добавить 
        }
    }
    public class HashTable
    {
        private int capacity;
        private int length;
        private HashTableNode[] elements;
        public HashTable()
        {
            capacity = 20;
            length = 0;
            elements = new HashTableNode[capacity];
            for (int i = 0; i < capacity; i++)
            {
                elements[i] = new HashTableNode();
            }
        }
        private int TestHash(TOP top) //!тест хеш = хеш1?
        {
            string cur = top.name + top.adress;
            var hash = 0;
            for (int i = 0; i < cur.Length; i++) 
            {
                hash += Convert.ToInt32(cur[i]); 
            }
            return hash % capacity;
        }

        private int SecondHash(int last_hash, int num) //квадратичный поиск
        {
            return (last_hash + Convert.ToInt32(Math.Pow(num + 1, 2))) % capacity;
        }

        public void Add(TOP newTOP)
        {
            var hash = TestHash(newTOP);
            var hash2 = hash;
            Console.WriteLine(hash);
            var step = 0; 
            while (elements[hash].state == 1 && step != capacity)
            {
                hash = SecondHash(hash2,step);
                step += 1;
                if (elements[hash].data == newTOP) { return; }
            }
            if (step == capacity)
            {
                Console.WriteLine("Didn't find the right place");
            }
            else
            {
                elements[hash].data = newTOP;
                elements[hash].key = newTOP.name + newTOP.adress;
                elements[hash].state = 1;
                length++;
                if ((double)length / capacity > 0.75) { Rehash(true); }
            }
        }

        public void Print()
        {
            for (int i = 0; i < capacity; i++)
            {
                Console.Write($" {elements[i]} ");
                Console.WriteLine(TestHash(elements[i].data));
            }
            Console.WriteLine();
        }
        public void Delete(TOP deletedUser)
        {
            var hash = TestHash(deletedUser); //изначальный хеш
            var hash2 = hash;
            var step = 0; //попытка равна нулю
            while ((elements[hash].state != 0 || elements[hash].data.name + elements[hash].data.adress != "") && step != capacity)
            {
                if (elements[hash].data == deletedUser) //если мы нашли, что удалять
                {
                    if (elements[hash].state == 1) //если состояние ячейки 1, то удаляем
                    {
                        elements[hash].state = 0;
                        length--;
                    }
                    int hash3 = SecondHash(hash2,step); //находим, где может быть следующий элемент
                    step += 1;
                    while (elements[hash3].state != 0 && step != capacity) //пока не попадаем в пустую ячейку
                    {
                        if (TestHash(elements[hash3].data) == hash2) //если хеш элемента после совпадает с удаленным
                        {
                            elements[hash].data = elements[hash3].data; //вставляем его в освободившееся место
                            elements[hash].state = 1;
                            elements[hash3].state = 0; //удалили
                            hash = hash3; //место вставки уже меняем
                        }
                        hash3 = SecondHash(hash2, step); //начальный хеш откуда удаляем + шаг
                        step += 1;
                    }
                    if ((double)length / capacity < 0.2 && capacity > 20)
                    { Rehash(false); }
                    return;
                }
                hash = SecondHash(hash2,step); //!везде поправить
                step += 1;
            }
        }
        public int Search(TOP soughtUser)
        {
            var hash = TestHash(soughtUser);
            var hash2 = hash;
            var step = 0;
            while ((elements[hash].state != 0 || elements[hash].data.name + elements[hash].data.adress != "") && step != capacity)
            {
                if (elements[hash].data == soughtUser) { return 0; }
                hash = SecondHash(hash2, step);
                step += 1;
            }
            return -1;
        }
        private void Rehash(bool increase)
        {
            length = 0;
            int newCapacity = increase ? capacity * 2 : capacity / 2;
            if (newCapacity < 20) { return; }
            var tmp = new HashTableNode[elements.Length];
            Array.Copy(elements, tmp, elements.Length);
            elements = new HashTableNode[newCapacity];
            for (int i = 0; i < newCapacity; i++)
            {
                elements[i] = new HashTableNode();
            }
            capacity = newCapacity;
            foreach (var node in tmp)
            {
                if (!(node.data is null) && node.data.name + node.data.adress != "" && node.state == 1)
                    Add(node.data);
            }

        }
    }

}